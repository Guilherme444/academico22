@extends('layouts.base')
@section('conteudo')

    <h5 class="row justify-content-center">LOGIN</h5>
    <div class="row justify-content-center">
      <img src="{{asset('imgs/login.png') }}" id="imgUser" />
    </div>
    <!-- form action="http://10.100.16.33/logar.php" method="post" -->
    <form action="{{ route('logado')}}" method="post" id="formLogin">
      @csrf
      <div class="form-group">
        <label for="usuario">Usuário:</label>
        <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Nome do Usuário">
      </div>

      <div class="form-group">
        <label for="senha">Senha:</label>
        <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha do usuário">
      </div>
      <button type="submit" class="btn btn-success">Logar</button>
    </form>
 @endsection